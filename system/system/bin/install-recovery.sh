#!/system/bin/sh
if ! applypatch --check EMMC:/dev/block/bootdevice/by-name/recovery:134217728:b5685f2e1f32ce7f1381eff06faadb62ebf15060; then
  applypatch  \
          --patch /system/recovery-from-boot.p \
          --source EMMC:/dev/block/bootdevice/by-name/boot:134217728:def58141920d13eb90b1347ee07085bde67807f1 \
          --target EMMC:/dev/block/bootdevice/by-name/recovery:134217728:b5685f2e1f32ce7f1381eff06faadb62ebf15060 && \
      log -t recovery "Installing new recovery image: succeeded" || \
      log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
